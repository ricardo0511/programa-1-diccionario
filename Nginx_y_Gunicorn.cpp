# nano
server {
    listen 80;
    server_name 127.0.0.1; 

    allow 127.0.0.1;
    allow 192.168.1.0/16;
    deny all;

    location = /static/favicon.svg { 
        root /home/<user>/<project_name>;
    }

    location /static/ {
        root /home/<user>/<project_name>;
    }

    location / {
        proxy_pass http://unix:/home/<user>/<project_name>/<project_name>.sock;
        include proxy_params;
    }
}