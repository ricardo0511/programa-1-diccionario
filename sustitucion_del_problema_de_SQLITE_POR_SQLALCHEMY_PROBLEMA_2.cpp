import sqlite3

NOMBRE_BASE_DE_DATOS = "diccionario.db"





def obtener_conexion():

    return sqlite3.connect(NOMBRE_BASE_DE_DATOS)





def crear_tablas():

    tablas = [

        """

        CREATE TABLE IF NOT EXISTS diccionario(

            id INTEGER PRIMARY KEY AUTOINCREMENT,

            palabra TEXT NOT NULL,

            significado TEXT NOT NULL

        );

        """

    ]

    conexion = obtener_conexion()

    cursor = conexion.cursor()

    for tabla in tablas:

        cursor.execute(tabla)