import time
import socket
import platform
import os
import os.path
import sys
import xlwt

print("Inventario equipo")
#datos en pantalla
print ('Sistema :', platform.system())
print ('Release :', platform.release())
print ('Version :', platform.version())
print ('Usuario :', os.getlogin())
print ('Nombre Maq.:', platform.node())
print ('Tipo Maq, :', platform.machine())
print ('Procesador :', platform.processor())
print ('Ip Maquina :', socket.gethostbyname(socket.gethostname()))

# creamos el fichero excel
wb = xlwt.Workbook()
# añadimos hoja
ws = wb.add_sheet('Mmi equipo')
# escribimos encabezados
ws.write(0,0,'Sistema')
ws.write(0,1,'Release')
ws.write(0,2,'Version')
ws.write(0,3,'Usuario')
ws.write(0,4,'Nombre Maq')
ws.write(0,5,'Procesador')
ws.write(0,6,'Ip Maquina')
# escribo columnas excel
col = 1
ws.write(col,0,platform.system())
ws.write(col,1,platform.release())
ws.write(col,2,platform.version())
ws.write(col,3,os.getlogin())
ws.write(col,4,platform.node())
ws.write(col,5,platform.machine())
ws.write(col,6,platform.processor())
ws.write(col,7,socket.gethostbyname(socket.gethostname()))
# grabo Fichero ecel.
print(" GEnerado Fichero ... Inventario_Equipo.xls")
wb.save('D:\Inventario_Equipo.xls')