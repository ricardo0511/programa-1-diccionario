desde el  matraz de  importación  Matraz , solicitud , jsonify
de  flask_sqlalchemy  importar  SQLAlchemy
de  flask_marshmallow  importar  Marshmallow

aplicación  =  Frasco ( __name__ )
aplicación . config [ 'SQLALCHEMY_DATABASE_URI' ] =  'mysql + pymysql: // root: faztpassword @ localhost / flaskmysql'
aplicación . config [ 'SQLALCHEMY_TRACK_MODIFICATIONS' ] =  Falso

db  =  SQLAlchemy ( aplicación )
ma  =  Marshmallow ( aplicación )


clase  Tarea ( db . Modelo ):
    id  =  db . Columna ( db . Entero , clave_primaria = Verdadero )
    título  =  db . Columna ( db . String ( 70 ), único = Verdadero )
    descripción  =  db . Columna ( cadena de db . ( 100 ))

    def  __init__ ( yo , título , descripción ):
        yo . title  =  titulo
        yo . description  =  descripción

db . create_all ()

clase  TaskSchema ( ma . Schema ):
    clase  Meta :
        campos  = ( 'id' , 'título' , 'descripción' )


task_schema  =  TaskSchema ()
tasks_schema  =  TaskSchema ( muchos = Verdadero )

@ aplicación . ruta ( '/ tareas' , métodos = [ 'Publicar' ])
def  create_task ():
  título  =  solicitud . json [ 'título' ]
  descripción  =  solicitud . json [ 'descripción' ]

  new_task =  Tarea ( título , descripción )

  db . sesión . agregar ( nueva_tarea )
  db . sesión . cometer ()

  return  task_schema . jsonify ( nueva_tarea )

@ aplicación . ruta ( '/ tareas' , métodos = [ 'OBTENER' ])
def  get_tasks ():
  all_tasks  =  Tarea . consulta . todo ()
  resultado  =  esquema_tareas . volcado ( all_tasks )
  devolver  jsonify ( resultado )

@ aplicación . ruta ( '/ tasks / <id>' , métodos = [ 'GET' ])
def  get_task ( id ):
  tarea  =  Tarea . consulta . obtener ( id )
  return  task_schema . jsonify ( tarea )

@ aplicación . ruta ( '/ tasks / <id>' , métodos = [ 'PUT' ])
def  update_task ( id ):
  tarea  =  Tarea . consulta . obtener ( id )

  título  =  solicitud . json [ 'título' ]
  descripción  =  solicitud . json [ 'descripción' ]

  tarea . title  =  titulo
  tarea . description  =  descripción

  db . sesión . cometer ()

  return  task_schema . jsonify ( tarea )

@ aplicación . ruta ( '/ tasks / <id>' , métodos = [ 'BORRAR' ])
def  delete_task ( id ):
  tarea  =  Tarea . consulta . obtener ( id )
  db . sesión . eliminar ( tarea )
  db . sesión . cometer ()
  return  task_schema . jsonify ( tarea )


@ aplicación . ruta ( '/' , métodos = [ 'OBTENER' ])
def  index ():
    return  jsonify ({ 'mensaje' : 'Bienvenido a mi API' })



if  __name__  ==  "__main__" :
    aplicación . ejecutar ( debug = True )